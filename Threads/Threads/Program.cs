﻿using System;
using System.Threading;

public class Program
{
    // Exercici 1
    public static string message1 = "Una vegada hi havia un gat";
    public static string message2 = "En un lugar de la Mancha";
    public static string message3 = "Once upon a time in the west";

    // Exercici 2
    public static int numCerveses = 0;
    public static Random random = new Random();
    public static int numRandom;

    static void Main()
    {
        //Exercici1();
        Console.WriteLine();
        Exercici2();
    }

    public static void Exercici1()
    {
        // Exercici 1
        bool threading = true;
        Thread thread2 = new Thread(() =>
        {
            var i = 0;
            while (threading && i == 0)
            {
                Write3(ref threading);
                i++;
            };
            if (!threading) Console.WriteLine("Thread 2 aborted");
        });
        Thread thread1 = new Thread(() =>
        {
            thread2.Join();
            Console.WriteLine(message1 + "  -- t1");
        });
        Thread thread3 = new Thread(() =>
        {
            thread2.Join();
            Console.WriteLine(message3 + "  -- t3");
        });

        thread1.Start();
        thread2.Start();
        thread3.Start();
    }

    public static void Write3(ref bool threading)
    {
        Thread.Sleep(2000);
        Console.WriteLine(message2 + "  -- t2");
    }

    public static void Exercici2()
    {
        // Exercici 2
        bool threading = true;
        Thread thread2 = new Thread(() =>
        {
            var i = 0;
            while (threading && i == 0)
            {
                OmplirNevera("Anitta");
                i++;
            };
            if (!threading) Console.WriteLine("Thread 2 aborted");
        });
        Thread thread1 = new Thread(() =>
        {
            thread2.Join();
            BeureCervesa("Bad Bunny");
        });
        Thread thread3 = new Thread(() =>
        {
            thread2.Join();
            BeureCervesa("Lil Nas X");
        });
        Thread thread4 = new Thread(() =>
        {
            thread2.Join();
            BeureCervesa("Manuel Turizo");
        });

        thread1.Start();
        thread2.Start();
        thread3.Start();
        thread4.Start();
    }

    private static void OmplirNevera(string namePerson)
    {
        numRandom = random.Next(7);
        if (numCerveses + numRandom > 9)
        {
            numRandom = 9 - numCerveses;
            numCerveses = 9;
        }
        else numCerveses += numRandom;

        Console.WriteLine(namePerson + " omple navera amb " + numRandom + " cerveses.");
    }

    private static void BeureCervesa(string namePerson)
    {
        numRandom = random.Next(numCerveses + 1);
        numCerveses -= numRandom;

        Console.WriteLine(namePerson + " es beu " + numRandom + " cerveses de la nevera.");

    }
}